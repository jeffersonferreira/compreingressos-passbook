class CreateQmPassbooks < ActiveRecord::Migration
  def change
    create_table :qm_passbooks do |t|
      t.string :pass_hash
      t.string :qrcode
      t.string :order_number
      t.string :pass
      t.text   :json

      t.timestamps null: false
    end
  end
end
