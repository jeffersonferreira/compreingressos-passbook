#encoding: utf-8

require 'fileutils'
require 'dubai'
require 'json'
require 'digest'

class PassController < ApplicationController

	##
	# Esta senha é compartilhada com o app iOS! Se mudar aqui será necessário mudar no app
	# e publicar novamente sincronizado com esta mudança!!
	PASSWORD = 'QMException *exception;'

	# JSON do pedido
	# {  
	#  	"number":"436464",
	#   "date":"sáb 28 nov",
	#   "total":"50,00",
	#   "espetaculo":{  
	#   "titulo":"COSI FAN TUT TE",
	#   	"endereco":"Praça Ramos de Azevedo, s/n - República - São Paulo, SP",
	#     "nome_teatro":"Theatro Municipal de São Paulo",
	#     "horario":"20h00"
	#   },
	#   "ingressos":[  
	# 	 	{  
	#       "qrcode":"0054741128200000100146",
	#       "local":"SETOR 3 ANFITEATRO C-06",
	#       "type":"INTEIRA",
	#    		"price":"50,00",
	#       "service_price":" 0,00",
	#       "total":"50,00"
	#     }
	#   ]
	# }

  def generate
    icon         = "#{public_path}/passbook/icon.png"
    icon_2x      = "#{public_path}/passbook/icon@2x.png"
    logo         = "#{public_path}/passbook/logo.png"
    logo_2x      = "#{public_path}/passbook/logo@2x.png"
    certificate  = "#{public_path}/passbook/compreingressos_passb00k.p12"
    order_number = params[:number]
    passes_count = params[:ingressos].count
    filenames    = []

    passes_count.times do |index|
      ingresso       = params[:ingressos][index]
      directory_name = ingresso[:qrcode]
      pass_directory = "#{temp_path}/#{directory_name}.pass"
      pkpass         = "#{directory_name}.pkpass"
      pkpass_path    = "#{temp_path}/#{directory_name}.pkpass"
      horario        = params[:espetaculo][:horario].gsub('h', ':')

      unless File.exists?(pass_directory)
        Dir.mkdir(pass_directory)

        FileUtils.cp(icon,    pass_directory)
        FileUtils.cp(icon_2x, pass_directory)
        FileUtils.cp(logo,    pass_directory)
        FileUtils.cp(logo_2x, pass_directory)

        # qrcode
        pass_dictionary = json_dictionary
        pass_dictionary["serialNumber"]       = ingresso[:qrcode]
        pass_dictionary["barcode"]["message"] = ingresso[:qrcode]
        pass_dictionary["barcode"]["altText"] = "Localizador - #{order_number}"

        # Descrição
        pass_dictionary["description"]        = "e-Ticket CompreIngressos para o evento #{params[:titulo]}"
        pass_dictionary["organizationName"]   = "e-Ticket CompreIngressos para o evento #{params[:titulo]}"

        # data
        pass_dictionary["eventTicket"]["headerFields"][0]["label"] = horario
        pass_dictionary["eventTicket"]["headerFields"][0]["value"] = params[:date]

        # nome do evento
        pass_dictionary["eventTicket"]["primaryFields"][0]["value"] = params[:espetaculo][:titulo]

        # teatro e endereço
        pass_dictionary["eventTicket"]["secondaryFields"][0]["label"] = params[:espetaculo][:nome_teatro]
        pass_dictionary["eventTicket"]["secondaryFields"][0]["value"] = params[:espetaculo][:endereco]

        # setor/assento
        pass_dictionary["eventTicket"]["auxiliaryFields"][0]["value"] = ingresso[:local]

        # preço
        pass_dictionary["eventTicket"]["auxiliaryFields"][1]["value"] = ingresso[:price]

        filename = "#{pass_directory}/pass.json"
        file = File.open(filename, 'w')
        file.write(pass_dictionary.to_json)
        file.close

        Dubai::Passbook.certificate, Dubai::Passbook.password = certificate, "qpromobile.compreingressos.n33Hb"
        File.open(pkpass_path, 'w') do |f|
          f.write Dubai::Passbook::Pass.new(pass_directory).pkpass.string
        end
      end

      filenames << pkpass
    end

    respond_to do |format|
      format.json   { render json: { passes: filenames } }
    end
  end

	def view
		filename = params[:id]
		filepath = "#{temp_path}/#{filename}.pkpass"
		respond_to do |format|		
			format.any  { send_file filepath, filename: 'pass.pkpass', type: 'application/vnd.apple.pkpass'}
		end
	end

	protected

		def temp_path
	 		"#{Rails.root}/tmp"
		end

		def public_path
			"#{Rails.root}/public"
		end

		def unique_string
			"#{rand(10000)}#{Time.now.to_f * 1000}".gsub('.', '')
 		end

 		def json_dictionary
 			{  
			  "description" => "Ingresso para um evento",
			  "formatVersion" => 1,
			  "organizationName" => "Q.PRO SOLUCOES LTDA",
			  "passTypeIdentifier" => "pass.com.qpromobile.compreingressos",
			  "serialNumber" => "145847e216b44dac8c58f79a27ca0008",
			  "teamIdentifier" => "8R6A865UP5",
			  "labelColor" => "rgb(255,255,255)",
			  "foregroundColor"  =>  "rgb(255,255,255)",
			  "backgroundColor"  =>  "rgb(208,17,43)",
			  "logoText" => "",
			   "barcode" => {  
			     "format" => "PKBarcodeFormatAztec",
			     "message" => "0054741128200000100147",
			     "messageEncoding" => "iso-8859-1",
			     "altText" => "Localizador - 436464"
			  },
			  "eventTicket" => {
			     "headerFields" => [
			        {
			           "key" => "date",
			           "label" => "20:00",
			           "value" => "29.04.2015"
			        }
			     ],
			     "primaryFields" => [
			        {
			           "key" => "eventName",
			           "label" => "Evento",
			           "value" => "COSI FAN TUT TE 2"
			        }
			     ],
			     "secondaryFields" => [
			        {
			           "key" => "location",
			           "label" => "Theatro Municipal de São Paulo",
			           "value" => "Praça Ramos de Azevedo, s/n - República, São Paulo - SP"
			        }
			     ],
			     "auxiliaryFields" => [
			      
			        {
			           "key" => "seatingSection",
			           "label" => "Setor/Assento",
			           "value" => "Setor 3 Anfiteatro - D-44"
			        },
			        {
			           "key" => "price",
			           "label" => "Valor",
			           "value" => "R$ 50,00"
			        }
			     ],
			     "backFields" => [
			        {
			           "key" => "info",
			           "label" => "Observações Importantes",
			           "value" => " - O espetáculo começa rigorosamente no horário marcado. Não haverá troca de ingresso, nem devolução de dinheiro em caso de atraso. Não será permitida a entrada após o inicio do espetáculo.\n - A taxa de serviço e os ingressos que forem adquiridos e pagos através desse canal não poderão ser devolvidos, trocado ou cancelados depois que a compra for efetuada pelo cliente e o pagamento confirmado pela instituição financeira.\n - É obrigatório apresentar o cartão utilizado na compra e um documento de identificação pessoal.\n - No caso de meia-entrada ou promoção é obrigatório a apresentação de documento que comprove o benefício no momento da retirada dos ingressos e na entrada do local.\n - Caso você tenha alguma dúvida sobre o seu pedido, entre em contato conosco através do e-mail: compreingressos@webdesklw.com.br ou acesse www.compreingressos.webdesklw.com.br"
			        },
			        {
			           "key" => "site",
			           "label" => "Site",
			           "value" => "www.compreingressos.com.br"
			        }        
			     ]
			  }
			}
 		end

end
