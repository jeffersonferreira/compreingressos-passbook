require 'open-uri'
require 'fileutils'

class V3::PassController < V2::PassController

  attr_accessor :error_message

  def generate
    begin
      if valid_payload?(params)
        filenames = generate_passes
        urls = filenames.map{ |filename| "#{path}/#{filename}" }
        render json: { passes: urls }
      else
        render json: { error: @error_message }, status: :unprocessable_entity
      end
    rescue => error
      logger.error error
      render json: { error: error }, status: :internal_server_error
    end
  end

  def view
    url = "#{path}/#{params[:id]}.pkpass"
    begin
      data = open(url).read
      respond_to do |format|
        format.any  { send_data data, filename: 'pass.pkpass', type: 'application/vnd.apple.pkpass'}
      end
    rescue Exception => e
      if e.message == '403 Forbidden'
        # Provavelmente ocorreu algum erro na hora de gravar o arquivo na amazon.
        # Neste caso vamos remover o diretório do pass aqui no heroku e esperar
        # que o usuário tente novamente na esperança de quando o pass for criado
        # novamente o erro na amazon não ocorra.
        pass_directory = "#{temp_path}/#{params[:id]}.pass"
        FileUtils.remove_dir pass_directory
      else
        puts $!.inspect, $@
      end
      respond_to do |format|
        format.any { render json: { error: e.message }}
      end
    end
  end

  private

    def path
      "https://s3-sa-east-1.amazonaws.com/#{ENV['AWS_S3_BUCKET']}/uploads"
    end

end