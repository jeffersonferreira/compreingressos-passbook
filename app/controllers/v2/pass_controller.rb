require 'open-uri'
require 'fileutils'

class V2::PassController < PassController

  attr_accessor :error_message

  def generate
    begin
      if valid_payload?(params)
        filenames = generate_passes
        render json: { passes: filenames }
      else
        render json: { error: @error_message }, status: :unprocessable_entity
      end
    rescue => error
      logger.error error
      render json: { error: error }, status: :internal_server_error
    end
  end

  def view
    path = "https://s3-sa-east-1.amazonaws.com/#{ENV['AWS_S3_BUCKET']}/uploads/#{params[:id]}.pkpass"
    begin
      data = open(path).read
      respond_to do |format|
        format.any  { send_data data, filename: 'pass.pkpass', type: 'application/vnd.apple.pkpass'}
      end
    rescue Exception => e
      if e.message == '403 Forbidden'
        # Provavelmente ocorreu algum erro na hora de gravar o arquivo na amazon.
        # Neste caso vamos remover o diretório do pass aqui no heroku e esperar
        # que o usuário tente novamente na esperança de quando o pass for criado
        # novamente o erro na amazon não ocorra.
        pass_directory = "#{temp_path}/#{params[:id]}.pass"
        FileUtils.remove_dir pass_directory
      else
        puts $!.inspect, $@
      end
      respond_to do |format|
        format.any { render json: { error: e.message }}
      end
    end
  end

  private

    def valid_payload?(params)
      if params[:number].blank?
        @error_message = 'order_number obrigatório'
        return false
      end
      passes_count = params[:ingressos].count
      passes_count.times do |index|
        ingresso = params[:ingressos][index]
        if ingresso[:qrcode].blank?
          @error_message = 'qrcode obrigatório'
          return false
        end
      end
      true
    end

    def generate_passes
      icon = "#{public_path}/passbook/icon.png"
      icon_2x = "#{public_path}/passbook/icon@2x.png"
      logo = "#{public_path}/passbook/logo.png"
      logo_2x = "#{public_path}/passbook/logo@2x.png"
      certificate = "#{public_path}/passbook/compreingressos_passb00k.p12"
      order_number = params[:number]
      passes_count = params[:ingressos].count
      filenames = []

      passes_count.times do |index|
        ingresso = params[:ingressos][index]
        hash_aux = "#{ingresso[:qrcode]}#{order_number}#{PASSWORD}"
        hash = Digest::SHA256.hexdigest(hash_aux)[0..39]
        directory_name = hash
        pass_directory = "#{temp_path}/#{directory_name}.pass"
        pkpass = "#{directory_name}.pkpass"
        pkpass_path = "#{temp_path}/#{directory_name}.pkpass"
        horario = params[:espetaculo][:horario].gsub('h', ':')

        # unless File.exists?(pass_directory)
          Dir.mkdir(pass_directory) unless Dir.exist?(pass_directory)

          FileUtils.cp(icon, pass_directory)
          FileUtils.cp(icon_2x, pass_directory)
          FileUtils.cp(logo, pass_directory)
          FileUtils.cp(logo_2x, pass_directory)

          # qrcode
          pass_dictionary = json_dictionary
          pass_dictionary['serialNumber'] = ingresso[:qrcode]
          pass_dictionary['barcode']['message'] = ingresso[:qrcode]
          pass_dictionary['barcode']['altText'] = "Localizador - #{order_number}"

          # Descrição
          pass_dictionary['description'] = "e-Ticket CompreIngressos para o evento #{params[:titulo]}"
          pass_dictionary['organizationName'] = "e-Ticket CompreIngressos para o evento #{params[:titulo]}"

          # horario
          horario ||= ''
          pass_dictionary['eventTicket']['headerFields'][0]['label'] = horario

          # data
          date = params[:date]
          date ||= ''
          pass_dictionary['eventTicket']['headerFields'][0]['value'] = date

          # nome do evento
          nome_evento = params[:espetaculo][:titulo]
          nome_evento ||= ''
          pass_dictionary['eventTicket']['primaryFields'][0]['value'] = nome_evento

          # teatro
          nome_teatro = params[:espetaculo][:nome_teatro]
          nome_teatro ||= ''
          pass_dictionary['eventTicket']['secondaryFields'][0]['label'] = nome_teatro

          # endereco
          endereco = params[:espetaculo][:endereco]
          endereco ||= ''
          pass_dictionary['eventTicket']['secondaryFields'][0]['value'] = endereco

          # setor/assento
          local = ingresso[:local]
          local ||= ''
          pass_dictionary['eventTicket']['auxiliaryFields'][0]['value'] = local

          # preço
          price = ingresso[:price]
          price ||= ''
          pass_dictionary['eventTicket']['auxiliaryFields'][1]['value'] = price

          filename = "#{pass_directory}/pass.json"
          file = File.open(filename, 'w')
          file.write(pass_dictionary.to_json)
          file.close

          Dubai::Passbook.certificate, Dubai::Passbook.password = certificate, 'qpromobile.compreingressos.n33Hb'
          File.open(pkpass_path, 'w') do |f|
            f.write Dubai::Passbook::Pass.new(pass_directory).pkpass.string
          end

          pass_file = File.open("#{temp_path}/#{pkpass}")
          model = QmPassbook.new
          model.pass_hash = hash
          model.qrcode = ingresso[:qrcode]
          model.order_number = order_number
          model.json = pass_dictionary.to_s
          model.pass = pass_file
          model.save!
        # end

        filenames << pkpass
      end
      filenames
    end

end