class QmPassbook < ActiveRecord::Base
  mount_uploader :pass, PassbookUploader

  def event_date_from_json
    groups = /[a-zá]{3} ([\d]{1,2} [a-z]{3})/.match(self.json)
    if groups
      str_date = groups[1]
      .gsub('mai', 'may')
      .gsub('jun', 'jun')
      .gsub('jul', 'jul')
      .gsub('ago', 'aug')
      .gsub('set', 'sep')
      .gsub('out', 'oct')
      .gsub('nov', 'nov')
      .gsub('dez', 'dec')
      begin
        return DateTime.strptime(str_date, '%d %b')
      rescue => e
        puts "pass:#{self.id} date:#{str_date} error:#{e.message}"
      end
    end
    nil
  end
end
